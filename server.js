'use strict';

const express = require('express')
const app = express()
const moment = require('moment')

function parseDate(date) {
  let dt = moment(date, 'YYYY-MM-DD-HH-mm-ss');
  return new Date(dt.format('YYYY-MM-DDTHH:mm:ss+00:00'));
}

// Database
var Snapshot = require('./models/orderbook').Snapshot;
var OrderBook = require('./models/orderbook').OrderBook;
var mongoose = require('mongoose');
{
    }
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost/orderbook', {
  useMongoClient: true,
    // sets how many times to try reconnecting
     reconnectTries: Number.MAX_VALUE,
    // sets the delay between every retry (milliseconds)
     reconnectInterval: 1000
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function()  {
  console.log('Connected to Mongo DB ...')
});

app.use(express.json())

app.get('/', (req, res) => res.send('Hello World'))

app.get('/orderbook/:symbol', (req, res, next) => {
  let symbol = req.params.symbol;
  let queryExecute;
  let begin,end;

  if (req.query.to) {
    end = moment(req.query.to, 'YYYY-MM-DD-HH-mm-ss');
  } else {
    end = moment();
  }

  if (req.query.from) {
    begin = moment(req.query.from, 'YYYY-MM-DD-HH-mm-ss');
  } else {
    if (req.query.seconds_ago) {
      begin = moment(end).subtract(Number(req.query.seconds_ago)*1000);
    }
  }

  let query = [
    {$match: {name: symbol}},
    {$unwind: '$data'}
  ]

  if(req.query.from || req.query.to || req.query.seconds_ago) {
    let dates = {}
    if (req.query.from || req.query.seconds_ago) {
      dates['$gte'] = parseDate(begin.format('YYYY-MM-DD-HH-mm-ss'))
    }
    if (req.query.to) {
      dates['$lt'] = parseDate(end.format('YYYY-MM-DD-HH-mm-ss'))
    }

    query.push({'$match': {'data.timestamp': dates}})
  }

  OrderBook.aggregate(query).then(function(book) {
      let count = book.length
      let status = count > 0 ? 'success' : 'no_data';
      let begin_str = begin ? begin.format('YYYY-MM-DD HH:mm:ss') : 'initial tick';
      let end_str = end ? end.format('YYYY-MM-DD HH:mm:ss') : 'last tick';

      res.json({
        status: status,
        data: book,
        message: 'Retrieved ' + count + ' orderbook snapshot(s) from ' + begin_str + ' to ' + end_str
      });
  }).catch(function(err) {
      console.log(err)
      next(err);
  })
})

app.get('/orderbook', (req, res, next) => {
  OrderBook.find().then(function(book) {
    res.json(book);
  }).catch(function(err) {
    next(err);
  })
})

app.post('/orderbook', (req, res, next) => {
  let data = req.body;
  let symbol = data.symbol.toLowerCase();

  OrderBook.findOneOrCreate({ name: symbol }, (err, book) => {
    if (err)
     next(err);

    let snapshot = new Snapshot(data.snapshot)
    snapshot.save().then(function(snapshot) {
        book.data.push(snapshot);
        book.save().then(function(book) {
        console.log('db save successful ...');
        res.json({status: 'success'});
      }).catch(function(err) {
         next(err);
      })
    }).catch(function(err) {
       next(err);
     })
  })
})

app.use(function(error, req, res, next) {
  res.status(400).json({ error: error.message })
})

app.listen(3002, () => console.log('Express app running ...'))
