var mongoose = require('mongoose')

var Price = Number;
var Volume = Number;

var Snapshot = new mongoose.Schema({
  timestamp: Date,
  exchange: String,
  bids: [[Price, Volume]],
  asks: [[Price, Volume]],
  trades: [[mongoose.Schema.Types.Mixed]]
})

var OrderBook = new mongoose.Schema({
  name: String,
  data: [Snapshot]
})

OrderBook.statics.findOneOrCreate = function findOneOrCreate(condition, callback) {
  const self = this;
  self.findOne(condition, (err, result) => {
    return result ? callback(err, result) : self.create(condition, (err, result) => {
      return callback(err, result)
    })
  })
}

module.exports = {
  Snapshot: mongoose.model('Snapshot', Snapshot),
  OrderBook: mongoose.model('OrderBook', OrderBook)
}
